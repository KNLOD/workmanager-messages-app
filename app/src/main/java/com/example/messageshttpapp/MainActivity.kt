package com.example.messageshttpapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.messageshttpapp.data.LocalDataRepository
import com.example.messageshttpapp.model.ListMessages
import com.example.messageshttpapp.model.Message
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        val showDataButton : Button = findViewById(R.id.show_data_button)
        showDataButton.isEnabled = false

        val viewModel: MessageViewModel by viewModels()

        val messages: MutableList<Message> = mutableListOf()

        // Observer setting current message in Adapter
        // To show editing dialog for it
        viewModel.message.observe(this, Observer {
            val dialog = MessageFragment()
            dialog.show(supportFragmentManager, "edit Message")
        })

        // Starting Service
        Intent(this, RequestService::class.java).also {
            Log.e("SERVICE", "Started")
            startService(it)
        }


        // Work requesets and constraints
        val constraints : Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED).build()
        val getRequest = OneTimeWorkRequest.Builder(RequestWorker::class.java)
            .setConstraints(constraints)
            .build()

        val workManager = WorkManager.getInstance(applicationContext)

        workManager.enqueue(getRequest)
        workManager.getWorkInfoByIdLiveData(getRequest.id).observe(this, Observer{
            if (it.state.isFinished) {
                // Load messages saved locally after completing Request
                Toast.makeText(applicationContext, "All messages are loaded!", Toast.LENGTH_SHORT).show()
                showDataButton.isEnabled = true
            }
        })

        showDataButton.setOnClickListener{

            val messages = LocalDataRepository().getMessages(applicationContext)
            recyclerView.adapter = MessageAdapter(messages, viewModel, applicationContext)
        }






        val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Companion.BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            // Observing submiting changed message
            // And POST it
            viewModel.changedMessage.observe(this, Observer{ message ->
                messages.set(message.id.toInt() - 1, message)

                val retrofitPost = retrofitBuilder.postMessages(ListMessages(messages))
                retrofitPost.enqueue(object: Callback<ListMessages> {

                    override fun onResponse(
                        call: Call<ListMessages>,
                        response: Response<ListMessages>
                    ) {

                        val responseBody = response.body()
                        // Show the HTTP response to ensure that everything
                        // ran right
                        Toast.makeText(applicationContext, "${response.raw()}", Toast.LENGTH_LONG).show()
                        Log.e("POST", "posted")
                        Log.e("POST", "${response.raw()}, ${response.headers()}")


                        // Check posted Data
                        responseBody?.let {
                            for (message in responseBody.messages) {
                                Log.e("POST", "posted: $message")
                            }
                        }


                    }

                    override fun onFailure(call: Call<ListMessages>, t: Throwable) {

                        Log.e("POST", "$call, $t")

                    }
                })

            })
        }
    }








