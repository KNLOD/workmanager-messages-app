package com.example.messageshttpapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputEditText

class MessageFragment : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_message, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val titleText : TextView = view.findViewById(R.id.title)
        val bodyEdit : TextInputEditText = view.findViewById(R.id.message_body_edit)

        val submitButton : Button = view.findViewById(R.id.submit_button)
        val cancelButton : Button = view.findViewById(R.id.cancel_button)

        val viewModel : MessageViewModel by activityViewModels()

        viewModel.message.observe(viewLifecycleOwner, Observer{
            titleText.text = it.title
            bodyEdit.setText(it.body)
        })

        submitButton.setOnClickListener{
            val changedMessage = viewModel.message.value?.copy(body = bodyEdit.text.toString())
            changedMessage?.let {
                viewModel.changeMessage(it)
            }
            dismiss()
        }

        cancelButton.setOnClickListener{
            dismiss()
        }

        super.onViewCreated(view, savedInstanceState)
    }


}