package com.example.messageshttpapp.model

data class Message(
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String
)