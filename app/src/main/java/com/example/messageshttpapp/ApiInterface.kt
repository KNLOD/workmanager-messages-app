package com.example.messageshttpapp

import com.example.messageshttpapp.model.ListMessages
import com.example.messageshttpapp.model.Message
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiInterface {
    @GET("posts")
    fun getMessages() : Call<List<Message>>

    @Headers("Content-Type: application/json")
    @POST("posts")
    fun postMessages(@Body messages : ListMessages)  : Call<ListMessages>



}