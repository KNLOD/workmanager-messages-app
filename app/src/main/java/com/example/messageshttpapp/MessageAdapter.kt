package com.example.messageshttpapp

import android.app.Activity
import android.app.Application
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.messageshttpapp.model.Message
import com.google.android.material.card.MaterialCardView


class MessageAdapter(val dataset : List<Message>, private val viewModel: MessageViewModel, val context: Context) : RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {


    class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val messageId : TextView = itemView.findViewById(R.id.message_id)
        val messageTitle : TextView = itemView.findViewById(R.id.message_title)
        val messageCard : MaterialCardView = itemView.findViewById(R.id.message_card)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType : Int) : MessageViewHolder{
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.message_list_item, parent, false)
        return MessageViewHolder(adapterLayout)


    }
    override fun onBindViewHolder(holder : MessageViewHolder, position: Int){
        //Log.e("MESSAGE ADAPTER", "${dataset[position].id}")
        holder.messageId.text = dataset[position].id.toString()
        holder.messageTitle.text = dataset[position].title
        // set current message to viewModel
        // to invoke dialog in MainActivity
        holder.messageCard.setOnClickListener{
            viewModel.setCurrentMessage(dataset[position])
        }
    }
    override fun getItemCount() : Int{
        //Log.e("MESSAGE ADAPTER", "$dataset")
        return dataset.size



    }


}