package com.example.messageshttpapp

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.messageshttpapp.model.Message
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RequestService : Service() {
    override fun onBind(p0: Intent?): IBinder? = null
    init{
        Log.e("SERVICE", "running")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(MainActivity.BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getMessages()
        // Get messages from URL
            retrofitData.enqueue(object : Callback<List<Message>?> {
                override fun onResponse(
                    call: Call<List<Message>?>,
                    response: Response<List<Message>?>
                ) {
                    val responseBody = response.body()
                    Log.d("SERVICE", "${response.body()}")

                    }




                override fun onFailure(call: Call<List<Message>?>, t: Throwable) {

                }
            })
        return START_NOT_STICKY


    }

}