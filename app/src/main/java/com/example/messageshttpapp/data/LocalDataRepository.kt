package com.example.messageshttpapp.data

import android.content.Context
import com.example.messageshttpapp.model.ListMessages
import com.example.messageshttpapp.model.Message
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import java.io.*


class LocalDataRepository {
    fun getMessages(applicationContext : Context) : List<Message>{
        val file = File(applicationContext.filesDir, MESSAGES_FILE)
        val fileReader = file.reader()
        val bufferedReader = BufferedReader(fileReader)
        val stringBuilder = StringBuilder()
        var line = bufferedReader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = bufferedReader.readLine()
        }
        bufferedReader.close()
        val messagesString = stringBuilder.toString()
        val jsonReader = JsonReader(StringReader(messagesString))
        jsonReader.isLenient = true
        //Log.e("JSON", messagesString)
        val messages = Gson().fromJson(messagesString, ListMessages::class.java)
        return messages.messages
    }
    fun saveMessagesLocally(applicationContext : Context, messages: List<Message>){

        val builder = GsonBuilder()
        val gson = builder.create()
        val messages_JSON = gson.toJson(ListMessages(messages))
        val file = File(applicationContext.filesDir, MESSAGES_FILE)
        val fileWriter = FileWriter(file)
        val bufferedWriter = BufferedWriter(fileWriter)
        bufferedWriter.write(messages_JSON)
        bufferedWriter.close()
    }
    companion object{
        const val MESSAGES_FILE = "messages.json"
    }
}