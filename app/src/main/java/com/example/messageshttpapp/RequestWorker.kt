package com.example.messageshttpapp

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.messageshttpapp.MainActivity.Companion.BASE_URL
import com.example.messageshttpapp.data.LocalDataRepository
import com.example.messageshttpapp.model.ListMessages
import com.example.messageshttpapp.model.Message
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

class RequestWorker(context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    companion object{
        val MESSAGES = "messages"
    }
    override suspend fun doWork() : Result{
        try{
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            val response = retrofitBuilder.getMessages().execute()
            // Get messages from URL

            val messages = mutableListOf<Message>()

            for (message in response.body()!!){
                Thread.sleep(100)
                messages.add(message)
                Log.d("MESSAGE", "${message.id}")
            }

            // Save loaded data locally
            LocalDataRepository().saveMessagesLocally(applicationContext, messages)

            return Result.success()
        } catch(e : Exception){
            return Result.failure()
        }
    }

}